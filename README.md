# Video Streaming Platform

## Core Features

Our platform is packed with features to enhance your live streaming sessions:

- **Streaming with RTMP/WHIP**: Leverage the power of RTMP and WHIP protocols for high-quality, low-latency video streaming.
- **Ingress Generation**: Easily generate ingress points for your live stream with just a few clicks.
- **OBS/Streaming Software Integration**: Connect your Next.js app directly to OBS or any streaming software of your choice.
- **Authentication**: Secure your streams with our robust authentication system.
- **Thumbnail Upload**: Customize your live stream with a unique thumbnail.
- **Live Viewer Count**: Keep track of how many viewers are watching your stream in real time.
- **Live Statuses**: Update and inform your viewers with live statuses throughout your stream.
- **Real-time Chat**: Engage with your audience through a real-time chat system, powered by sockets.
- **Unique Color for Each Viewer in Chat**: Assign a unique color to each viewer in chat to easily identify messages.
- **Following System**: Allow users to follow their favorite streamers for instant notifications on live sessions.
- **Blocking System**: Manage your community by blocking disruptive viewers.
- **Kicking Participants**: Maintain the quality of your stream by kicking participants in real-time if necessary.
- **Streamer/Creator Dashboard**: Access a comprehensive dashboard to manage your streams, engage with viewers, and analyze your performance.

### Install packages

```shell
npm i
```

### Setup .env file


```js
NEXT_PUBLIC_CLERK_PUBLISHABLE_KEY=
CLERK_SECRET_KEY=
NEXT_PUBLIC_CLERK_SIGN_IN_URL=/sign-in
NEXT_PUBLIC_CLERK_SIGN_UP_URL=/sign-up
NEXT_PUBLIC_CLERK_AFTER_SIGN_IN_URL=/
NEXT_PUBLIC_CLERK_AFTER_SIGN_UP_URL=/
CLERK_WEBHOOK_SECRET=

DATABASE_URL=

LIVEKIT_API_URL=
LIVEKIT_API_KEY=
LIVEKIT_API_SECRET=
NEXT_PUBLIC_LIVEKIT_WS_URL=

UPLOADTHING_SECRET=
UPLOADTHING_APP_ID=
```
